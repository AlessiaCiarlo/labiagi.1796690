HOMEWORK2
La cartella caricata contiene tutti i file presenti nella cartella turtlesim del workspace ROS.
I file creati da me sono:
 - circle_service  -> contiene SwapCircle, GetCircles e RemoveCircle
 - draw_circle_client   -> contiene il client per la creazione dei cerchi
 - remove_circle_client -> contiene il client per l'eliminazione dei cerchi 

ISTRUZIONI PER L'USO
1. Dopo aver creato un workspace ROS e, al suo interno, una cartella "src", copiare la qui presente cartella "turtlesim" 
   qui   --->   workspace/src/ros_tutorials
2. Da terminale recarsi nel workspace ed eseguire catkin_make 
		(verranno creati gli eseguibili di tutti i servizi)
3. Passi per eseguire l'homework:
   - aprire un terminale e lanciare il comando ~$ roscore 
   - aprire un terminale e lanciare il comando ~$ rosrun turtlesim turtlesim_node 
		(per aprire la finestra con una tartaruga)
   - aprire un terminale e lanciare il comando ~$ rosrun turtlesim circle_service
   - aprire un terminale e lanciare il comando ~$ rosrun turtlesim draw_circle_client 
		(per creare N cerchi/tartarughe)
   - aprire un terminale e lanciare il comando ~$ rosrun turtlesim turtle_teleop_key 
		(per muovere la tartaruga centrale da tastiera)
   - aprire un terminale e lanciare il comando ~$ rosrun turtlesim remove_circle_client 
		(per rimuovere i cerchi/tartarughe colpite dalla tartaruga in movimento)
 
OSSERVA! eseguire *~$ source ~/workspaces/nome_workspace/devel/setup.bash* prima di ogni lancio di servizio 

--------------------------------------------------------------------------------------------------

HOMEWORK5
Lanciare da terminale il programma signals.py con il seguente comando ~$ python3 signals.py
Svolgerà in automatico il training e il testing di un riconoscitore di segnali stradali

--------------------------------------------------------------------------------------------------

HOMEWORK6
I file creati da me sono:
 - move  -> contiene movebase_client.cpp
 - cmakelists

ISTRUZIONI PER L'USO
1. Dopo aver creato un workspace ROS e, al suo interno, una cartella "src", copiare la qui presente cartella "move" 
   qui   --->   workspace/src
2. Da terminale recarsi nel workspace ed eseguire catkin_make 
		(verranno creati gli eseguibili di tutti i servizi)
3. Passi per eseguire l'homework:
   - aprire un terminale e lanciare il comando ~$ roscore 
   - aprire un terminale, lanciare ~$ roscd stage_ros/
				   ~$ rosrun stage_ros stageros world/willow-erratic.world
   - aprire un terminale e lanciare il comando ~$ rosrun map_server map_server mappa.yaml 
		(per caricare una mappa già precedentemente creta)
   - aprire un terminale e lanciare il comando ~$ rosrun thin_navigation thin_localizer_node
   - aprire un terminale e lanciare il comando ~$ rosrun thin_navigation thin_planner_node
   - aprire un terminale per far partire rviz con ~$ rviz
   - aprire un terminale e lanciare il comando ~$ rostopic echo /base_pose_ground_truth 
		(per conoscere la posizione reale di partenza del robot) 
   - aprire un terminale e lanciare il comando ~$ rostopic pub /initialpose TAB TAB 
		(e inserire i valori di partenza scoperti nel passo precedente)
   - aprire un terminale e lanciare il comando ~$ rosrun move move 
		(per lanciare il client che invierà posizioni al robot)
  
OSSERVA! eseguire *~$ source ~/workspaces/nome_workspace/devel/setup.bash* prima di ogni lancio di servizio ros


