#include <ros/ros.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>

#define WIN1 "color_window"
#define WIN2 "gray_window"
#define WIN3 "blurred_window"
#define WIN4 "edge_window"

#define low 100
#define high 250

void imageCallback(const sensor_msgs::CompressedImageConstPtr& msg) {

  cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
  try {
    cv::imshow(WIN1, cv_ptr->image); 
    cv::waitKey(40);
  }
  catch (cv_bridge::Exception& e) {
    ROS_ERROR("Could not convert to image");
  }

  cv::Mat gray_img;
  cv::cvtColor(cv_ptr->image, gray_img, CV_BGR2GRAY);
  /*try {
    cv::imshow(WIN2, gray_img); 
    cv::waitKey(20);
  }
  catch (cv_bridge::Exception& e) {
    ROS_ERROR("Could not convert to gray_img");
  }*/

  cv::Mat blurred_img;
  cv::GaussianBlur(gray_img, blurred_img, cv::Size(5, 5), 0);
  /*try {
    cv::imshow(WIN3, blurred_img); 
    cv::waitKey(20);
  }
  catch (cv_bridge::Exception& e) {
    ROS_ERROR("Could not blur gray_img");
  }*/

  cv::Mat edges_img;
  cv::Canny(blurred_img, edges_img, low, high, 3);
  try {
    cv::imshow(WIN4, edges_img); 
    cv::waitKey(40);
  }
  catch (cv_bridge::Exception& e) {
    ROS_ERROR("Could not detect edges");
  }

}
  
int main(int argc, char **argv) {
  ros::init(argc, argv, "image_listener");
  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe("/default/camera_node/image/compressed", 1, imageCallback);
  ros::spin();
}