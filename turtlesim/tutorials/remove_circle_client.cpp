#include <ros/ros.h>
#include <cstdlib>
#include <iostream>
#include <turtlesim/RemoveCircle.h>
#include <turtlesim/GetCircles.h>
#include <turtlesim/Pose.h>
#include <vector>

std::vector<turtlesim::Circle> circles;

bool controlCallback(const turtlesim::PoseConstPtr& m) {

  //creo il nodo per chiamare la Get e la Remove
  ros::NodeHandle n1;
  ros::ServiceClient client_get = n1.serviceClient<turtlesim::GetCircles>("get_circles"); 
  turtlesim::GetCircles::Request g_req;
  turtlesim::GetCircles::Response g_res;
  
  //chiamo la getcircle del server
  if (client_get.call(g_req, g_res)) {
      ROS_INFO("chiamata la get \n");
  }
  else {
      ROS_ERROR("Failed to call service get \n");
      return 1;
  }

  circles = g_res.circles;

  ros::ServiceClient client_remove = n1.serviceClient<turtlesim::RemoveCircle>("remove_circle"); 
  turtlesim::RemoveCircle::Request r_req;         
  turtlesim::RemoveCircle::Response r_res; 
  
  float delta = 0.5;
  int num_elem = circles.size();

  std::cout << num_elem << "\n";

  //controllo la presenza di altre tartarughe 
  for (int i = 0; i < num_elem; i++) {
    if ((((circles[i].x >= m->x - delta) && (circles[i].x <= m->x + delta)) && 
         ((circles[i].y >= m->y - delta) && (circles[i].y <= m->y + delta)))) {

      r_req.id = circles[i].id;

      //chiamo la removecircle del server
      if (client_remove.call(r_req, r_res)) {
        ROS_INFO("chiamata la remove \n");
      }
      else {
        ROS_ERROR("fallita la chiamata a remove \n");
        return 1;
      }
    }
  }
  return true;
}


int main(int argc, char **argv) {
  
  ros::init(argc, argv, "remove_circle_client");
  
  ros::NodeHandle n;
  ros::Subscriber control_sub = n.subscribe<turtlesim::Pose>("turtle1/pose", 1, controlCallback);

  ROS_INFO("creato il nodo \n");

  ros::spin();
}
