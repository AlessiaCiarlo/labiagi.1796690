#include <ros/ros.h>
#include <cstdlib>
#include <iostream>
#include <turtlesim/Spawn.h>
#include <turtlesim/SpawnCircle.h>
#include <turtlesim/RemoveCircle.h>
#include <turtlesim/GetCircles.h>
#include <turtlesim/Kill.h>
#include <turtlesim/Pose.h>
#include <vector>

#define N 20
#define B 9
#define A 1

ros::Publisher pub;
std::vector<turtlesim::Circle> circles;
std::vector<std::string> turtles;

bool spawnCallback(turtlesim::SpawnCircle::Request &req, turtlesim::SpawnCircle::Response &res) { 	//SpawnCircle ...........function called whenever a message is received

    //creo il nodo per chiamare la Spawn 
    ros::NodeHandle n;
    ros::ServiceClient server_client = n.serviceClient<turtlesim::Spawn>("spawn");
    turtlesim::Spawn::Request s_req;
    turtlesim::Spawn::Response s_res;

    float x, y, theta;
    int i;
    turtlesim::Circle c;   //singolo cerchio
    
    x = req.x;
    y = req.y;

    for (i=0; i<N; i++) {
        
        //creo il cerchio in posizione i-sima dell'array 
        c.id = i;
        c.x = rand() % B + A;
        c.y = rand() % B + A;
        circles.push_back(c);

        ROS_INFO("richiesta: x=%f, y=%f", (float)req.x, (float)req.y);
        ROS_INFO("creato il cerchio: [%d]", c.id);

        //chiamata di spawn
        s_req.x = c.x;                    
        s_req.y = c.y;
        s_req.theta = (s_req.x + s_req.y)/2;  
        
        if (server_client.call(s_req, s_res)) {
            ROS_INFO("Nuova tartaruga in finestra \n");
        }
        else {
            ROS_ERROR("Tartaruga mai nata \n");
            return 1;
        }

        turtles.push_back(s_res.name);
        std::cout << "aggiunta tartaruga " << s_res.name << "\n";

        //incremento x e y per avere tartarughe più sparse 
        x += 1;
        y += 1;
    }

    res.circles = circles;        //restituisco i cerchi

    int num_elem = circles.size();
    for (int i = 0; i < num_elem; i++) {
        std::cout << circles[i] << turtles[i] << "\n";
    }

    return true;		
}

bool getCallback(turtlesim::GetCircles::Request &req, turtlesim::GetCircles::Response &res) {   
    res.circles= circles;
    return true;
}

bool removeCallback(turtlesim::RemoveCircle::Request &req, turtlesim::RemoveCircle::Response &res) {
    
    //creo il nodo per chiamare il servizio Kill
    ros::NodeHandle n1;
    ros::ServiceClient server_kill = n1.serviceClient<turtlesim::Kill>("kill");
    turtlesim::Kill::Request k_req;
    turtlesim::Kill::Response k_res;

    int id = req.id;
    int num_elem = circles.size();

    //elimino il cerchio con id = req.id
    for (int i = 0; i < num_elem; i++) {
        if (circles[i].id == id) {
            circles.erase(circles.begin() + i);
            ROS_INFO("elimiato il cerchio: [%d] \n", id);
            break;
        }
    }

    //chiamata di kill
    k_req.name = turtles[id];
    if (server_kill.call(k_req, k_res)) {
        ROS_INFO("Tartaruga Turtle%d uccisa \n", id);
    }
    else {
        ROS_ERROR("Fallimento in chiamata di kill \n");
        return 1;
    }

    res.circles = circles;  //restituisco i cerchi rimanenti 
    return true;
}


int main(int argc, char** argv){
    
    ros::init(argc, argv, "draw_circle_service"); 		//inizializzo ros
    
    ros::NodeHandle nodo;					//creo il nodo server
 
    //avverto che ho dei servizi attivi su quel nodo 
    ros::ServiceServer s = nodo.advertiseService("spawn_circles", spawnCallback); 
                      
    ros::ServiceServer s1 = nodo.advertiseService("get_circles", getCallback); 
                         
    ros::ServiceServer s2 = nodo.advertiseService("remove_circle", removeCallback);

    ros::spin();     
}