#include "ros/ros.h"
#include <cstdlib>
#include <iostream>
#include <turtlesim/SpawnCircle.h>

int main(int argc, char **argv) {
  
  ros::init(argc, argv, "draw_circle_client");
  
  ros::NodeHandle nodo;
  ros::ServiceClient client = nodo.serviceClient<turtlesim::SpawnCircle>("spawn_circles");   //SpawnCircle

  ROS_INFO("creato il nodo \n");

//Creao request e response 
  turtlesim::SpawnCircle srv;
  turtlesim::SpawnCircle::Request req;         
  turtlesim::SpawnCircle::Response res;        

  req.x = 3;  //atoll(argv[1]);
  req.y = 1;  //atoll(argv[2]);

//chiamo la spawncircle del server
  if (client.call(req, res)) {
    ROS_INFO("chiamato il server \n");
  }
  else {
    ROS_ERROR("Failed to call service \n");
    return 1;
  }
}