#include "ros/ros.h"
#include <cstdlib>
#include <iostream>
#include <move_base/move_base.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>


typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

bool moveCallback(const geometry_msgs::PoseWithCovarianceStampedConstPtr& m) {

  MoveBaseClient client("move_base", true);    //true -> don't need ros::spin()
  
  //wait for the action server to come up
  while(!client.waitForServer(ros::Duration(20.0))){
    ROS_INFO("Waiting for the move_base action server");
  }

  float x = m->pose.pose.position.x;
  float y = m->pose.pose.position.y;

  move_base_msgs::MoveBaseGoal goal;

  //we'll send a goal to the robot
  goal.target_pose.header.frame_id = "base_link";
  goal.target_pose.header.stamp = ros::Time::now();
  
  goal.target_pose.pose.position.x = -15.0;
  goal.target_pose.pose.position.y = 23.0;
  
  ROS_INFO("Sending goal");
  client.sendGoal(goal);
  
  client.waitForResult(ros::Duration(60.0));

  client.cancelAllGoals();

  //we'll send a goal to the robot to move 
  goal.target_pose.header.frame_id = "base_link";
  goal.target_pose.header.stamp = ros::Time::now();
  
  goal.target_pose.pose.position.x = x;
  goal.target_pose.pose.position.y = y;

  ROS_INFO("Sending goal");
  client.sendGoal(goal);
  
  client.waitForResult(ros::Duration(60.0));
  if(client.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    ROS_INFO("The base moved");
  else
    ROS_INFO("The base failed to move for some reason");

  return true;

}


int main(int argc, char **argv) {
  
  ros::init(argc, argv, "movebase_client");

  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe<geometry_msgs::PoseWithCovarianceStamped>("amcl_pose", 100, moveCallback);

  ROS_INFO("creato il nodo \n");

  ros::spin();

  return 0;
}
 