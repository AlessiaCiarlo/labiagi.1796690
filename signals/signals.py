import numpy as np   #supporto per matrici
import matplotlib    #supporto per creazione grafici
import matplotlib.pyplot as plt
import os  
os.environ['CUDA_VISIBLE_DEVICES'] = '3'

# keras-imports per il dataset e la rete neurale
from keras.preprocessing.image import ImageDataGenerator
from keras.datasets import mnist
from keras.models import Sequential, load_model
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import np_utils
from keras import backend as K


# dimensioni delle immagini
img_width, img_height = 64, 64

train_data_dir      = '/home/kitsune/Downloads/DITS-classification/classification train'
validation_data_dir = '/home/kitsune/Downloads/DITS-classification/classification test'
train_samples       = 7489
validation_samples  = 1159
epochs              = 20
batch_size          = 128


# datagenerator
train_datagen = ImageDataGenerator(
	rescale         = 1. / 255, 
	shear_range     = 0.2, 
	zoom_range      = 0.2, 
	horizontal_flip = True)
test_datagen = ImageDataGenerator(rescale= 1. / 255)

train_generator = train_datagen.flow_from_directory( 
	directory   = train_data_dir,
	shuffle     = True,
    target_size = (img_width, img_height),
    color_mode  = "rgb",
    batch_size  = batch_size,
    class_mode  = "categorical")

validation_generator = test_datagen.flow_from_directory(
    directory   = validation_data_dir,
    target_size = (img_width, img_height),
    color_mode  = "rgb",
    batch_size  = batch_size,
    class_mode  = "categorical")


#layer sequenziali 
model = Sequential()
 
model.add(Conv2D(16, 3, padding = 'same', input_shape = (64, 64, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size = (2, 2)))

model.add(Conv2D(32, 3, padding = 'same'))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size = (2, 2)))

model.add(Conv2D(64, 3, padding = 'same'))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size = (2, 2)))

model.add(Flatten())
model.add(Dense(512))
model.add(Dropout(0.2))
model.add(Dense(59))
model.add(Activation('softmax'))


# compilo il modello
model.compile(loss      ='categorical_crossentropy', 
              metrics   =['accuracy'], 
              optimizer ='adam')

# training del modello
history = model.fit(
		train_generator,
        shuffle          = True,
        steps_per_epoch  = train_samples//batch_size,
        epochs           = epochs,
        validation_data  = validation_generator,
        validation_steps = validation_samples//batch_size)


# salvataggio del modello
save_dir   = "/home/kitsune/results/"
model_name = 'signals.h5'
model_path = os.path.join(save_dir, model_name)
model.save(model_path)
print('Saved trained model at %s ' % model_path)

# plotting delle metriche
fig = plt.figure()
plt.subplot(2,1,1)
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='lower right')

plt.subplot(2,1,2)
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper right')

plt.tight_layout()

fig


#valutazione
signal_model = load_model(model_path)
scores = signal_model.evaluate_generator(
                validation_generator, 
                validation_samples//batch_size, 
                verbose = 2)
print("Loss: ", scores[0])
print("Accuracy: ", scores[1])

plt.show()

